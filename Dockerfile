FROM ubuntu:18.04 as base

LABEL bio_node=v1.0 \
      bionode_entrypoint=/app/bionode.sh \
      input="sra_id|sra_list_file" \
      output="fastq_files" \
      maintainer=mmiller@bromberglab.org \
      description="NCBI sra-toolkit optimization for faster and more reliable (batch) downloads of SRA read files"

# setup system
RUN apt-get update && apt-get install -y \
    wget \
    curl \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

FROM base as builder

# setup app
COPY . /app

RUN useradd -ms /bin/bash builder && \
    mkdir /install && chmod 777 /install

USER builder

WORKDIR /tmp
RUN mkdir /install/bin && \
    mkdir /install/share && \
    wget -q --output-document src1.tar.gz http://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/current/sratoolkit.current-ubuntu64.tar.gz && \
    tar -xzf src1.tar.gz && \
    SRA=$(ls | grep sratoolkit) && \
    cp -r $SRA/bin/* /install/bin && \
    wget -q --output-document src2.tar.gz https://download.asperasoft.com/download/sw/connect/3.9.6/ibm-aspera-connect-3.9.6.173386-linux-g2.12-64.tar.gz && \
    tar -xzf src2.tar.gz && \
    ASPERA=$(ls | grep ibm-aspera-connect) && \
    bash $ASPERA > /dev/null && \
    mv ~/.aspera /install/share/aspera

FROM base

COPY --from=builder /install /usr/local
COPY --from=builder /app /app
COPY default.kfg /usr/local/bin/ncbi/default.kfg

# set environment variables
WORKDIR /app
ENV INPUT_PATH=/input OUTPUT_PATH=/output

# set project ENTRYPOINT
ENTRYPOINT ["bash", "evenfasterq-dump.sh"]

# set project CMD
# CMD []
